# Makefile to install material-modeling
#
# just type: make help        for help about install
# just type: make install     to install the tool itself
# just type: make install_web to install the web service
#
# required: (make deps)
#  apache2 ase gpaw gpaw-data libapache2-mod-perl2 libnet-ldap-perl
#  libproc-background-perl libsys-cpuload-perl libsys-cpu-perl openmpi-bin
#  python3-ase python3-h5py python3-matplotlib python3-numpy python3-phonopy
#  python3-psutil python3-scipy python3-spglib python3-toml python3-xraylib
#  python3-yaml quantum-espresso python3-plotly
#
# and get from https://packages.debian.org/sid/quantum-espresso-data-sssp
#   quantum-espresso-data-sssp 
#
# NOTES:
# - You may have to tune the path to /etc/apache2/conf-available/material-modeling.conf
#   or other locations. 
# - Configure the web service at /usr/lib/cgi-bin/material-modeling.pl and its
#   login page at /var/lib/material-modeling/README.html
# - Once installed, connect to http://localhost/material-modeling/
#
# make deps
#   install Debian/Ubuntu dependencies (before make install)
# make deps_web
#   install Debian/Ubuntu dependencies (before make install)
# make install
#   install material-modeling tool (not the web service)
# make install_web
#   install material-modeling-service (web service)
# make test
#   test

all:
  # nothing to do. Use 'make help|install|install_web'

# dependencies

help:
	@help2man bin/material-modeling -n "Compute electronic and vibrational properties of materials" > doc/material-modeling.1      || echo "WARNING: skip man page (missing help2man)"
	@pandoc README.md --to pdf > doc/README.pdf || echo "WARNING: skip pdf doc (missing pandoc)"
	@echo "Usage: install material-modeling"
	@echo "make all"
	@echo "  show help"
	@echo "make deps"
	@echo "  install Debian/Ubuntu dependencies (before make install)"
	@echo "make deps_web"
	@echo "  install Debian/Ubuntu dependencies (before make install_web)"
	@echo "make install"
	@echo "  install script and web service"
	@echo "make install_script"
	@echo "  install material-modeling tool (not the web service)"
	@echo "make install_web"
	@echo "  install material-modeling-service (web service)"
	@echo "make deb"
	@echo "  create debian package"

deps:
	sudo apt install ase openmpi-bin python3-ase python3-h5py python3-matplotlib python3-numpy python3-psutil python3-scipy python3-spglib python3-toml python3-yaml quantum-espresso
	sudo apt install python3-phonopy python3-xraylib python3-plotly || echo "WARNING: skip phonopy/xraylib"
	sudo apt remove quantum-espresso-data
	sudo apt install quantum-espresso-data-sssp || echo "Reinstall quantum-espresso-data package OR get https://packages.debian.org/sid/quantum-espresso-data-sssp"
	@echo " "
	@echo "INFO: now use: make install_script"

deps_web:
	sudo apt install apache2 libapache2-mod-perl2 libnet-ldap-perl libproc-background-perl libsys-cpuload-perl libsys-cpu-perl
	@echo " "
	@echo "INFO: now use: make install_web"

# installation
install: install_script install_web

install_script:
	install -D -d $(DESTDIR)$(prefix)/usr/share/doc/material-modeling
	install -D -d $(DESTDIR)$(prefix)/usr/share/doc/material-modeling/examples
	install -D -d $(DESTDIR)$(prefix)/usr/bin
	install -D bin/material-modeling $(DESTDIR)$(prefix)/usr/bin/
	install -D -m 644 examples/* $(DESTDIR)$(prefix)/usr/share/doc/material-modeling/examples
	install -D -m 644 README.md $(DESTDIR)$(prefix)/usr/share/doc/material-modeling
	install -D -m 644 doc/README.pdf $(DESTDIR)$(prefix)/usr/share/doc/material-modeling
	install -D -m 644 doc/material-modeling.1 $(DESTDIR)$(prefix)/usr/share/man/man1/material-modeling.1
	chmod 755 $(DESTDIR)$(prefix)/usr/bin/material-modeling
	@echo " "
	@echo "You can now use material-modeling script"

install_web:
	install -D -d $(DESTDIR)$(prefix)/usr/share/material-modeling/
	install -D -d $(DESTDIR)$(prefix)/usr/share/material-modeling/images
	install -D -d $(DESTDIR)$(prefix)/var/lib/material-modeling/data/
	ln -sf /var/lib/material-modeling/data/ $(DESTDIR)$(prefix)/usr/share/material-modeling/
	install -D -m 644 html/material-modeling.html $(DESTDIR)$(prefix)/usr/share/material-modeling/index.html
	install -D -m 644 images/* $(DESTDIR)$(prefix)/usr/share/material-modeling/images
	install -D cgi-bin/material-modeling.pl $(DESTDIR)$(prefix)/usr/lib/cgi-bin/material-modeling.pl
	install -D -m 644 html/apache.conf $(DESTDIR)$(prefix)/etc/apache2/conf-available/material-modeling.conf
	# configure accounts and permissions
	chmod a+xr $(DESTDIR)$(prefix)/var/lib/material-modeling
	find $(DESTDIR)$(prefix)/usr/share/material-modeling -type f -exec chmod a+r {} +
	find $(DESTDIR)$(prefix)/usr/share/material-modeling -type d -exec chmod a+rx {} +
	install -D -m 644 html/apache.conf $(DESTDIR)$(prefix)/etc/apache2/conf-available/material-modeling.conf
	# handle Debian build
	if [ "$(id -u)" -eq 0 ] ; then \
	adduser --system --home /var/lib/material-modeling --force-badname _material-modeling; \
	chown _material-modeling /var/lib/material-modeling; \
	chown _material-modeling /var/lib/material-modeling/data; \
	a2enmod cgi; \
	a2enconf material-modeling; \
	service apache2 reload; \
	fi
	@echo "INFO: Configure service at $(DESTDIR)$(prefix)/usr/lib/cgi-bin/material-modeling.pl"
	@echo "INFO: Configure layout  at $(DESTDIR)$(prefix)/usr/share/material-modeling/index.html"
	@echo "INFO: Connect to           http://localhost/material-modeling/"

test:
	bin/material-modeling examples/Al.cif
	@echo Test OK

deb: help
	cp -r packaging/debian .
	debuild -b || true
	-rm -rf debian

uninstall:
	-rm -rf $(DESTDIR)$(prefix)/usr/share/material-modeling
	-rm -rf $(DESTDIR)$(prefix)/usr/share/doc/material-modeling
	-rm -f  $(DESTDIR)$(prefix)/usr/lib/cgi-bin/material-modeling.pl
	-rm -f  $(DESTDIR)$(prefix)/usr/bin/material-modeling
	-rm -f  $(DESTDIR)$(prefix)/usr/share/man/man1/material-modeling.*
	-rm -rf  $(DESTDIR)$(prefix)/var/lib/material-modeling
	deluser _material-modeling

.PHONY: all install uninstall

