#!/usr/bin/perl -w

# E. Farhi (c) Synchrotron SOLEIL GPL3
# https://gitlab.com/soleil-data-treatment/soleil-software-projects/material-modeling-service
use strict;
use CGI; # see https://metacpan.org/pod/CGI
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
use Net::LDAP;          # libnet-ldap-perl          for ldap user check
use File::Copy;
use File::Path;
use Sys::CPU;           # libsys-cpu-perl           for CPU::cpu_count
use Sys::CpuLoad;       # libsys-cpuload-perl       for CpuLoad::load
use Proc::Background;   # libproc-background-perl   for Background->new

# ensure all fatals go to browser during debugging and set-up
# comment this BEGIN block out on production code for security
BEGIN {
    $|=1;                               # auto-flush stdout
    use CGI::Carp('fatalsToBrowser');
}

# use https://perl.apache.org/docs/2.0/api/Apache2/RequestIO.html
# for flush with CGI
my $r = shift;
if (not $r or not $r->can("rflush")) {
  push @ARGV, $r; # put back into ARGV when not a RequestIO object
}

use constant IS_MOD_PERL => exists $ENV{'MOD_PERL'};
use constant IS_CGI      => IS_MOD_PERL || exists $ENV{'GATEWAY_INTERFACE'};

$CGI::POST_MAX        = 100*1024; # in bytes, default 100kB. Update html on change.
$CGI::DISABLE_UPLOADS = 0;        # 1 disables uploads, 0 enables uploads

# see http://honglus.blogspot.com/2010/08/resolving-perl-cgi-buffering-issue.html
#CGI->nph(1);

# CONFIG: adapt values to your needs
my $www_root      = "/var/lib/material-modeling";  # root of the web server
my $executable    = "material-modeling";  # the name of the python script executable
my $target        = "data";   # the results sub-directory on the server, shown also in URLs.
my $ldap_uri      = '';       # e.g. 'ldap://195.221.10.1' or 'ldap://example.com:389' or ''
my $ldap_base     = 'dc=EXP'; # e.g. 'dc=EXP' or 'dc=example,dc=com' or ''
my $service_max_load = 0.8;   # deny service when server load is above
my $service_mpi_nb= 'all';    # max nb of cores to use with codes that use MPI.

# get the form
my $form          = new CGI;
my $error         = "";

if (Sys::CpuLoad::load() / Sys::CPU::cpu_count() > $service_max_load) {
    $error .= "Server load exceeded. Try again later. ";
  }

# test form consistency
my @fields   = ( 'user_id','smearing','calculator','dataset','compute' );

# test if the form has the required fields
for (@fields) {
  if ( not defined $form->param($_) ) {
    $error .= "Incomplete form: fields are missing: $_ ";
  }
}
# test if required information is there
for ('user_id') {
  if (not $form->param($_)) {
    $error .= "Some required (*) information is missing: $_ ";
  }
}
# test for valid credentials (when LDAP is defined)
my $authenticated = session_check_ldap(
                      $ldap_uri, $ldap_base,
                      $form->param('user_id'), $form->param('user_pw'));
if (index($authenticated, "FAILED") > -1) {
  $error .= $authenticated;
}

# get a valid dataset_name (will be a directory)
my $safe_filename_characters    = "a-zA-Z0-9_.-";
my $user_id      = $form->param('user_id');
my $dataset      = $form->param('dataset');
my $calculator   = $form->param('calculator');
my $smearing     = $form->param('smearing');
my $compute      = $form->param('compute');
my $io_handle    = $form->upload('dataset');
my $dataset_name = $dataset;
$dataset_name    =~ s/[^$safe_filename_characters]//g; # remove illegal characters
$user_id         =~ s/[^$safe_filename_characters]//g; # remove illegal characters

my $servername    = $form->server_name();
if ($servername =~ "::1") { $servername = "localhost"; }

# we create a target directory: $dataset_name_$calculator_$user_id_$date
my( $sec, $min, $hour, $mday, $mon, $year ) = localtime;
my $datestamp = sprintf "%04d%02d%02d-%02d%02d%02d",
    $year+1900, $mon+1, $mday, $hour, $min, $sec;

my $directory = "$dataset_name-$calculator-$user_id-$datestamp";

# we copy the uploaded file into the target
if (not $error) {
  if (defined $io_handle) {
    File::Path::make_path("$www_root/$target/$directory");  # make sure target exists
    # move the temp file to the storage we use
    if (not copy($form->tmpFileName( $io_handle ), "$www_root/$target/$directory/$dataset_name")) {
      $error .= "Can not copy $dataset into $directory. ";
    }
  } else {
    $error .= "Can not copy $dataset into $directory (no handle). ";
  }
}

# we create a configuration file to pass parameters
if (open (FH, '>>', "$www_root/$target/$directory/config.toml")) {
  print FH "# file generated by $0 on " . localtime() . "\n";
  print FH "#log=\"$www_root/$target/$directory/$executable-log.txt\"\n"; # done at the $cmd level, below
  print FH "atoms=\"$www_root/$target/$directory/$dataset_name\"\n";
  print FH "calculator=\"$calculator\"\n";
  print FH "directory=\"$www_root/$target/$directory/\"\n";
  print FH "smearing=\"$smearing\"\n";
  print FH "compute=\"$compute\"\n";
  print FH "to_all=\"True\"\n";
  print FH "user_id=\"$user_id\"\n";
  print FH "user_ip=\"" . $ENV{'REMOTE_HOST'} . " [" . $ENV{'REMOTE_ADDR'} . "]\"\n";
  if ($service_mpi_nb) {
    print FH "mpi=\"$service_mpi_nb\"\n";
  }
  close(FH);
} else {
  $error .= "Can not create the configuration file. ";
}

if (not $error) {
  # create the README file if it does not exist yet
  open (FH, '>>', "$www_root/$target/$directory/README.html") or die $!;
  print FH "\n";
  close(FH);
  
  chmod 0644, "$www_root/$target/$directory/README.html";
}

# launch it in background
if (not $error) {
  # NOTE: 
  # - Go into the target directory for any temporary file.
  #
  #   In some cases, the calculation may stop with messages such as:
  #     "failed to open /dev/dri/renderD128: Permission denied "
  #     "No protocol specified"
  # - Any call to MPI goes through SSH, which often has a 'ForwardX11 yes'.
  # - Inactivate any DISPLAY to avoid opening an X through MPI/SSH calls
  # - Inactivate openGL with HWLOC_COMPONENTS=-gl (see https://github.com/open-mpi/ompi/issues/7701)
  my $cmd = "cd $www_root/$target/$directory ; "
  . "DISPLAY= HWLOC_COMPONENTS=-gl $executable --config $www_root/$target/$directory/config.toml "
  . ">> $www_root/$target/$directory/$executable-log.txt 2>&1 &";
  print STDERR "$0: INFO: $cmd\n";
  
  my $proc = Proc::Background->new($cmd); 
  if (not $proc) {
    $error  .= "Could not start the computation for $dataset_name ";
  } else {
    sleep(10);
    my $url = $ENV{'HTTP_ORIGIN'} . "/$executable/$target/$directory/";
    print $form->redirect($url);
    # the 1st argument of CGI is an Apache RequestIO
    if (defined($r) and $r->can("rflush")) { 
      $r->rflush; 
    }
  }
} 
if ($error) {
  print $form->header ( ); # wrap up in html
  print "<h1>ERROR starting material modeling</h1>\n";
  print "<h2>\n";
  print "<p><div style='color:red'>$error</div></p>";
  print "</h2>\n";
  print "</body></html>";
}

# the 1st argument of CGI is an Apache RequestIO
if (defined($r) and $r->can("rflush")) { 
  $r->rflush; 
}

exit;



# ------------------------------------------------------------------------------
# session_check_ldap((ldap_server, ldap_base, user, password):
#   input:
#     ldap_server: an IP/URI        e.g. 'ldap://195.221.10.1:389'
#     ldap_base:   a search string, e.g. 'dc=EXP'
#     user:        a user name,     e.g. 'farhie'
#     password:    the user pw
#   return ""         when no check is done
#          "FAILED"   when authentication failed
#          "SUCCESS"  when authentication succeeded
sub session_check_ldap {
  my $ldap_server = shift;
  my $ldap_base   = shift;
  my $user        = shift;
  my $password    = shift;
  my $res = '';
  
  if (not $ldap_server or not $ldap_base) { return ""; }

  if (not $user) {
    return "FAILED: Missing Username.";
  }
  
  my $ldap = Net::LDAP->new($ldap_server)
    or return "FAILED: Cannot connect to LDAP server '$ldap_uri': $@";
    
  # identify the DN
  my $mesg = $ldap->search(
    base   => "$ldap_base",
    filter => "cn=$user", # may also be "uid=$user"
    attrs  => ['dn','mail','cn','uid']);
  
  if (not $mesg or not $mesg->count) {
    $res = "FAILED: Empty LDAP search.\n";
  } else {
    foreach my $entry ($mesg->all_entries) {
      my $dn    = $entry->dn();
      my $cn    = $entry->get_value('cn'); # may also be 'uid'
      my $bmesg = $ldap->bind($dn,password=>$password);
      if ( $bmesg and $bmesg->code() == 0 ) {
        $res = "SUCCESS: $user authenticated.";
      }
      else{
        my $error = $bmesg->error();
        $res = "FAILED: Wrong username/password (failed authentication): $error\n";
      }
    }
  }
  $ldap->unbind;
  return $res;
  
} # session_check_ldap

