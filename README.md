# material-modeling-service

A command and web service that computes electronic and vibrational band structures for material modeling of condensed matter crystals.

![Overview](images/banner.png)

Table of contents:

- [Description](#description)
- [Some theory](#intensity-estimates)
- [Installation](#installation)
- [Usage as a command](#usage-as-a-command)
- [Usage as a web service](#usage-as-a-web-service)
- [Example: SrTiO3](#example-srtio3)
- [Example: LaB6](#example-lab6)
- [Performance and Credits](#performance)

--------------------------------------------------------------------------------

## Description

This is a script and a web service that:

- load a CIF structure file (e.g. inorganic crystal).
- allow to specify the calculation type (electronic, vibrational, calculator).
- use ASE and PhonoPy transparently.
- estimate electronic states using the DFT method;
- estimate vibrational dispersions (harmonic) using the small-displacement method;
- estimate X and neutron vibrational coherent inelastic cross-sections.
- return 2D/4D structure bands and plots.

The calculation of [electronic states](https://en.wikipedia.org/wiki/Electronic_band_structure) is made in the density-functional theory (DFT), and the vibrational properties are obtained in the small displacement method (D. Alfe 2009 ; A. Togo 2023). Most DFT settings are automatically set.

### Features

The service can be used as a single command and from a web page.

The following calculation steps (`-c COMP` option) can be combined:

- `electrons`: just the ground state, the electron density of states, and the electronic band structure along the default k-path in the Brillouin Zone (BZ).
- `electrons_3d`: same as above, and adds the electronic dispersions in the full BZ. 
- `phonons`: computes the density of states and band structure along the default k-path in the Brillouin Zone (BZ) for phonons (harmonic). The intensity ($F(Q)^2$ and $S(Q, \omega)$) for neutrons and X-rays are computed.
- `phonons_3d`: same as above, and in addition the dispersions in the full BZ will be computed for phonons (harmonic). The intensity ($F(Q)^2$ and $S(Q, \omega)$) for neutrons and X-rays are computed.

All results are available as a report and a `properties.h5` HDF5 file as well as separate data files in common formats.

<img src="images/electronic_band_structure.svg" title="LaB6 e bands" height=128> 
<img src="images/electronic_dos.svg"            title="LaB6 edos" height=128> 
<img src="images/phonon_band_structure.svg"     title="LaB6 vbands" height=128> 
<img src="images/phonon_dos.svg"                title="LaB6 vdos" height=128> 
<img src="images/phonon_band_structure_3d_Sqw_powder_neutron.svg" title="SrTiO3 Sqw" height=128>
<img src="images/SrTiO3-arpes.png" title="SrTiO3 ARPES" height=128>

### Intensity estimates

#### Intensity of vibrational modes

For the phonons, the structure factor $F_j$ is computed for each vibrational mode $j$, as:

$F_j(Q) = \sum_a \frac{b_a}{\sqrt{m_a}} e^{-W_j(Q)} \vec{Q}.\vec{e_j} e^{-i \vec{Q}.\vec{r}}$

where the summation is over the atoms $a$, the Debye-Waller function is defined as:

$W_j(Q)= \frac{|\vec{Q}.\vec{e_j}|^2}{2 \omega_j(Q) m_a} (2 n(\omega_j(Q)+1)$

and the Bose population function is:

$n(\omega) = 1 / (e^{\hbar \omega / k_B T} - 1)$

where $T$ is the temperature, $k_B$ the Boltzmann constant. The atomic scattering length $b$ is $b_c$ (coherent) for neutrons (in fm), and $r_e f(E_i)$ for X-rays with $r_e = 2.817$ fm being the Thomson classical radius of the electron and $f(E_i)$ the atomic form factor that converges to the number of electrons $Z$ at high X-ray incident energy $E_i$.

The $|F_j(Q)|^2$ value is stored in the `F2_` items in the results.
The observed mode intensity is the sum over all modes:

$S(\vec{Q},\omega) = \sum_j (n(\omega) + 1) \frac{|F_j(Q)|^2}{\omega_j} DHO(\omega, \omega_j, \Gamma)$

where we assume the phonon line-shape to be a damped harmonic oscillator centred on $\omega_j(Q)$. The half-width $\Gamma$ can be specified with the `--qgamma` option (see below).

:warning: The intensity on the X and neutron cross-sections on the 1st Brillouin may be small for some modes and directions, due to the $\vec{Q}.\vec{e_j}$ scalar product. It is thus recommended to estimate the vibrational dispersions on further HKL BZ, just as in an experiment, using the `-q=HKL` option (see below).

:warning: When the temperature $T$ is defined, the computed cross-sections include the phonon population, so that the final distribution is _quantum_ in the sense that it contains the Bose factor and is not symmetric in energy. To get classical/symmetric $S(q,\omega)$ estimates, use the option `-t 0` or `-s temperature=0`.

References:

- PHON, D. Alfe, Comp Phys Comm 180 (2009) 2622.DOI: 10.1016/j.cpc.2009.03.010
- PhonoPy A. Togo, J. Phys.: Condens. Matter 35 353001 (2023). DOI: 10.1088/1361-648X/acd831
- H. Schober, JNR 17 (2014) 109–357. DOI: 10.3233/JNR-140016

#### Intensity of angle resolved photo emission spectroscopy (ARPES)

We may compute an estimate of the ARPES intensity from the raw electronic dispersions $E(k)$. 

The transition probability is derived from the Fermi golden rule between the initial and final electron states, coupled by the magnetic vector potential **A**.:

$\Gamma_{i \rightarrow f} \propto |<f|A.p|i>|^2 \delta(E_f-E-h \nu)$

where $E$ is the electronic band energy, $E_f$ is the Fermi level energy, and $h \nu$ is the incident photon energy. In practice, the momentum $p$ is the in-plane electron momentum, as the photon interacts with electrons at the sample surface. Also, in the dipole approximation, the tensor $A$ is simplified as the photon polarization $\epsilon$. Last, the electronic population follows a Fermi-Dirac law for fermions.

For the calculation, we define $\vec{u}$ as a normal vector to the scattering surface, in the reciprocal space. 
Then the perpendicular component of the electron scattering momentum $q$ is: 

$\vec{q}_{\perp} = \frac{\vec{q}.\vec{u}}{|\vec{u}|} \vec{u}$ 

and the in-plane component is:

$\vec{p} = \vec{q}_{||} = \vec{q} - \vec{q}_{\perp}$.

The ARPES intensity is then approximated as the product of:

- The dipole selection rule, which is the square of the scalar product of the photon polarization $\vec{\epsilon}$ with the in-plane momentum $|\vec{\epsilon}.\vec{q}_{||}|^2$.
- The one-electron removal spectral function, which is approximated as a Lorentzian line-shape centered of the electronic energy. We use as default a constant line width broadening $\Gamma$.
- The Fermi-Dirac population $f(E) = \frac{1}{1+e^{(E-E_F)/kT)}}$.

So that we get the expression:

$I(q,E) \propto \sum_{bands} |\vec{\epsilon}.\vec{q}_{||}|^2 \frac{1}{1+e^{(E-E_F)/kT)}} \frac{1}{1+(E - E_f)^2/\Gamma^2}$

In practice, we can estimate the ARPES intensity using the raw electron momentum $q$ on the whole BZ, and extract the relevant surface scattering by selecting $q$ values that match the surface cut plane.

:warning: This methodology is only meant to provide a **crude estimate**. 
Indeed, it does not consider any band level splitting, 
and assumes the final state to be a single electron extracted from the atom with N-1 unperturbed electrons (which is obviously false).

References:

- [C. Dösinger, 2021](https://gitlab.com/-/project/48157779/uploads/0f88e1a43bc56c76ef122197dceda84c/Masterarbeit_Christoph-Doesinger.pdf)
- https://en.wikipedia.org/wiki/Angle-resolved_photoemission_spectroscopy

### Looking for materials: data bases

You may find many crystal structures in the following databases:

- https://optimadeclient.materialscloud.io/ (catalog of catalogs)
- https://alexandria.icams.rub.de/ (2.5 M structures and calculations, via optimade)
- https://www.materialscloud.org/home (QuantumEspresso team)
- https://nomad-lab.eu/nomad-lab/ (simulated structures)
- https://next-gen.materialsproject.org/ (simulated structures)
- https://www.crystallography.net/cod/ (simulated and exp. structures)
- https://github.com/atztogo/phonondb (phonopy db)

--------------------------------------------------------------------------------

## Installation

We provide Debian/Ubuntu packages to setup everything in one single click. See the [releases page](https://gitlab.com/soleil-data-treatment/soleil-software-projects/material-modeling-service/-/releases).

#### Requirements

The single computation script is in pure python, and can in principle be used directly from anywhere, as long as the following dependencies are pre-installed:
```
ase openmpi-bin python3-ase python3-h5py python3-matplotlib python3-numpy python3-psutil python3-scipy python3-spglib python3-toml python3-yaml quantum-espresso
# optionally (recommended)
python3-phonopy python3-xraylib python3-plotly
```

In case you plan to use Quantum Espresso (QE), we recommend that you also install the SSSP pseudo-potentials from e.g. any of:

- https://packages.debian.org/sid/quantum-espresso-data-sssp
- `apt install quantum-espresso-data-sssp`

In this case, we also recommend that you _remove_ the default QE pseudo-potentials with `sudo apt remove quantum-espresso-data` to avoid conflicts and only leave the SSSP dataset.

For Debian-class systems (Debian/Ubuntu/Mint/...), you may install these with:
```
sudo make deps
```

You may additionally install other calculators, e.g. `gpaw`, `vasp`, `elk-lapw`, but this is not required as QuantumEspresso is an excellent choice.
```
sudo apt install gpaw elk-lapw
```

The optional web service relies on the Apache web server. Its installation is slightly more involved (see below). It requires the following dependencies:
```
apt install apache2 libapache2-mod-perl2 libnet-ldap-perl libproc-background-perl libsys-cpuload-perl libsys-cpu-perl
```

For Debian-class systems (Debian/Ubuntu/Mint/...), you may install these with:
```
sudo make deps_web
```

#### Installation: computational script

As stated above, the `material-modeling` script can be directly used, without specific need for a system-level installation. In this way, there is no need for an actual installation.

For a system-level installation (for all users, e.g. a server), the easiest is to make use of the automatic installation for the script, with:
```
sudo make install
```

In case `phonopy` is not available, the default, slower, ASE methods will be used.
In case `xraylib` is not available, only the neutron intensities will be computed.

You may then use the `material-modeling` script for any supported calculation.

- `material-modeling -h`
- `material-modeling -s` to list all settings

See below for examples.

#### Installation: web service

The installation assumes we can install an Apache web server.

Just run the commands:
```
sudo make install       # if not done already
sudo make install_web
```
Alternatively, you may copy the `html/material-modeling.html` file e.g. as `/var/www/html/material-modeling/index.html` and activate the cgi apache module.

You should adapt the `/usr/lib/cgi-bin/material-modeling.pl` configuration section (e.g. path to hold calculation results, any LDAP server).
```perl
# CONFIG: adapt values to your needs
my $www_root      = "/var/www/html";  # root of the web server
my $executable    = "material-modeling";  # the name of the python script executable
my $target        = "data/$executable";   # the results sub-directory on the server, shown also in URLs.
my $ldap_uri      = '';       # e.g. 'ldap://195.221.10.1' or 'ldap://example.com:389' or ''
my $ldap_base     = 'dc=EXP'; # e.g. 'dc=EXP' or 'dc=example,dc=com' or ''
my $service_max_load = 0.8;   # deny service when server load is above
my $service_mpi_nb= 'all';    # max nb of cores to use with codes that use MPI.
```

If you require an LDAP user authentication, uncomment the relevant section in the `/var/www/html/material-modeling/index.html`.

You may then use the `material-modeling` web service at (also accessible remotely):

- http://localhost/material-modeling/

--------------------------------------------------------------------------------

## Usage as a command

```
# compute Al electronic states
material-modeling /usr/share/material-modeling/examples/Al.cif

# compute electronic modes in 1st BZ
material-modeling examples/Al.cif -c electrons_3d

# compute Al vibrational modes on default -1:1:21 HKL cube.
material-modeling examples/Al.cif -c phonons_3d

# compute Al vibrational modes using fast EMT, on -2:2:41 HKL cube.
material-modeling examples/Al.cif -c phonons_3d -q="-2,2,41" -s calculator=emt
```

The lattice cell structure file, in any format handled by [ASE](https://wiki.fysik.dtu.dk/ase/ase/io/io.html), should better be the primitive lattice cell, to speed-up calculations. 
Also make sure to check that the space-group is properly set, not defaulting to a low symmetry (which makes calculations much slower).

The common options are:
```
  -a, --to-all          Generate all data files (default is HDF5, HTML and SVG).
  -c COMPUTE, --compute COMPUTE
                        Properties to compute, e.g.
                        'optimize+electrons+electrons_3d+phonons+phonons_3d'.
  -q QPOINTS3D, --qpoints3d QPOINTS3D
                        HKL 3D grid to evaluate vibrational levels (default:
                        -1:1:21). Can be given as scalar (N^3 bins), NPY/TXT 
                        filename, 3 values 'min,max,N', 6 values
                        'hmin,hmax,kmin,kmax,lmin,lmax', 9 values
                        'hmin,hmax,Nh,kmin,kmax,Nk,lmin,lmax,Nl'.
  --qgamma QGAMMA       Vibrational modes line width, constant when positive
                        [meV], proportional when negative. Default is w/20
                        [meV].
  -o DIR, --output DIR, --dir DIR
                        Write results into directory (append mode). When
                        already exists, re-use calculation steps.
  -e XENERGY, --energy XENERGY
                        Incident X-ray energy [keV] for the vibrational
                        intensity estimate.
  -s [KEY=VALUE ...], --set [KEY=VALUE ...]
                        Comma separated key=value pairs of settings, e.g.
                        calculator, compute, occupations, kpoints,
                        supercell... When no KEY is given, the list of
                        available settings is shown. This option must come
                        LAST.

```

**NOTE***: The `-q=` option is especially useful to estimate the X/neutron cross-section above the 1st BZ. In this case, specify the HKL cube as 3 or 9 values (`min,max,N` for each HKL - default is `-1,1,21`), with e.g.:
```
material-modeling examples/Al.cif   -c phonons_3d -q='-2,2,41' -s calculator=emt
material-modeling examples/LaB6.cif -c phonons_3d -q='-2,2,41'
```

The result of the computations is stored in a directory (can be set with `-o DIR` option or `-s directory=DIR`) which holds all data files (see e.g. `properties.h5`), and a `README.html` report page. 
In case the target directory already exists, previous calculation steps will be re-used when possible. 
You may selectively generate data files in a variety of formats with options such as `--to-svg`, or all formats with option `-a` and `--to-all`. 
The default is to generate HDF5, HTML report and SVG images.

data format | option      | comment
------------|-------------|-------------
ALL         | `--to-all`  | generate all data file formats.
HDF5        | `--no-hdf5` | do NOT generate HDF5 files, except for `properties.h5`. Use a [silx](https://www.silx.org/) to view.
HTML        | `--no-html` | do NOT generate HTML report. Use a browser to view.
JSON        | `--to-json` | generate JSON files (rather slow).
MAT         | `--to-mat`  | generate Matlab/Octave files.
NPY         | `--to-npy`  | generate Pytho Numpy NPY files. Use e.g. [silx](https://www.silx.org/) to view.
NRRD        | `--to-nrrd` | generate NRRD files. Use e.g. [paraview](https://www.paraview.org/) to view.
McCode      |`--to-mccode`| generate [McStas](https://mcstas.org/)/[McXtrace](https://mcxtrace.org/) files for X/neutron virtual experiments. Use `-t 0` to get classical/symmetric S(q,w).
PNG         | `--to-png`  | generate PNG images.
SVG         | `--no-svg`  | do NOT generate SVG images.
TOML        | `--to-toml` | generate TOML files (rather slow).

The default computational settings are to use the QuantumEspresso calculator, some sensible DFT parameters, and to compute the electronic properties only. 
To compute the vibrational properties, you should specify e.g.:

```
material-modeling examples/Al.cif -c phonons+electrons
```

It is possible to specify more settings, such as the calculator, with KEY=VALUE option:
```
material-modeling examples/Al.cif -s calculator=EMT
```

The script currently works with: 

`-s calculator=C` | comment
------------------|-------------------
 QuantumEspresso  | fast, this is the default
 GPAW             | slow, usually runs on a single core via ASE
 VASP             | rather fast and accurate (commercial)
 EMT              | for testing: only H, C, N, O, Al, Ni, Cu, Pd, Ag, Pt and Au, only for phonons
 ELK-LAPW         | very slow
 AbInit           | may fail

If your crystal is known to be a metal or insulator, set the smearing (`-s occupations=eV`) as such, else leave it in 'auto' (default).

The list of possible KEY=VALUE pairs is:

  KEY         | VALUE
--------------|------------------------------------------------------------------
  atoms       | A filename describing the Atoms lattice cell (CIF, POSCAR, PDB, ...). See [ASE formats](https://wiki.fysik.dtu.dk/ase/ase/io/io.html) for supported formats.
  calculator  | ESPRESSO, GPAW, ELK, EMT, VASP, and experimentally ABINIT.
  compute `-c`| A string that specifies what to compute among `optimize+electrons+electrons_3d+phonons+phonons_3d` (default: `electrons`)
  command     | path to calculator executable (default: auto).
  convergence | Threshold on the energy for self-consistency (default: 1e-5 eV).
  directory `-o`| Directory where all should be stored (append, default: `/tmp/band_structure_XXX`).
  displacement| Displacement of atoms in Angs to compute the phonons (default: 0.01 [Angs])
  ecut        | Kinetic energy cutoff for wave-functions (default: 200*n_typ_atoms in [eV]).
  eigensolver | Eigensolver diagonalization 'dav' (fast) or 'cg' (slow) or 'rmm-diis' (default: 'rmm-diis').
  kpoints     | Monkhorst-Pack grid for electronic ground state (default: auto=(kpoints3d^3/nb_at/supercell)^(1/3)).
  kpoints3d `-k`| HKL 3D grid to evaluate electronic levels (default: `-0.5,0.5,11`). Can be given as scalar, NPY/TXT filename, 3 values `min,max,N`, 6 values `hmin,hmax,kmin,kmax,lmin,lmax`, 9 values `hmin,hmax,Nh,kmin,kmax,Nk,lmin,lmax,Nl`. Large binning (> 1000) results in very long calculations.
  label       | Calculation name (default: chemical formula extracted from the lattice cell).
  log         | A file name where to send the messages (default: sent to stdout).
  maxiter     | Max number of iterations for SCF (default: infinite);
  mpi         | Number of MPI processes to use. Use 0 for no-MPI (use OpenMP), 'all', or a scalar (default: 'all');
  nbands      | Number of valence bands for which wave-functions are being computed (default: all).
  occupations | 'metal' 0.27 eV for metals ('smearing') help converge; 'insulator' -1 for insulators; 'semiconductor'    0.01 eV Fermi-Dirac smearing; 'auto' 0.13 eV (automatic smearing); or a value in eV for the distribution width e.g. 0.3.
  potentials  | Path to pseudo potentials aka setups (default: auto).
  qgamma      | Vibrational modes line width, constant when positive [meV], proportional when negative. Default is w/20 [meV].
  qpoints3d `-q`| HKL 3D grid to evaluate vibrational levels (default: `-1,1,21`). Can be given as scalar, NPY/TXT filename, 3 values `min,max,N`, 6 values `hmin,hmax,kmin,kmax,lmin,lmax`, 9 values `hmin,hmax,Nh,kmin,kmax,Nk,lmin,lmax,Nl`.
  supercell   | Supercell size (default: auto=(kgrid^3/nb_at)^(1/6)).
  temperature `-t`| Material temperature in [K] (default: 300 K). Use T=0 to disable Bose factor (n=1).
  wpoints `-w`| Energy binning for S(q,w) intensity estimates (scalar or 'min,max,N', default: 50).
  xc          | Type of Exchange-Correlation functional to use: 'LDA','PBE','revPBE','RPBE','PBE0','B3LYP'   for GPAW; 'LDA','PBE','revPBE','RPBE','GGA'            for ABINIT; 'LDA','PBE','PW91'                           for VASP. Default is 'PBE'.
  xray_energy `-e`| X-ray incident energy [keV] for the estimate of the F2 and S(q,w).


The full command options are obtained with:
```
material-modeling -h
material-modeling -s # to list default settings
```

## Usage as a web service

![The service landing page](images/landing_page.png)

Simply connect to (also accessible remotely):

- http://localhost/material-modeling/

The web service allows to choose the computation to perform among:

- **electrons only**: just the ground state, the electron density of states, and the electronic band structure along the default k-path in the Brillouin Zone (BZ).
- **electrons, 3d**: same as above, and adds the electronic dispersions in the full BZ. Use e.g. `slix view` and open the `electronic_band_structure_3d.h5` file, and double-click its `FREQ' item. Then select the 'cube' representation in the lower bar, and set the 4th dimension to be the unnamed axis (frequency). Iso-surfaces in the HKLE space are shown.
- **electrons and phonons**: computes the density of states and band structure along the default k-path in the Brillouin Zone (BZ) for both electrons and phonons.
- **electrons and phonons, 3d**: same as above, and in addition the dispersions in the full BZ will be computed for both electrons and phonons. Use [silx](https://www.silx.org/) as above to view the HKLE cubes in HDF5 and NPY formats.

If your crystal is known to be a metal or insulator, set the smearing as such, else leave it in 'auto'.


All results are stored e.g. at (also accessible remotely):

- http://localhost/data/material-modeling

and shown as web pages with data files.

![The results page](images/results.png)

We recommend that you make use of e.g. [silx](https://www.silx.org/) (HDF5, NPY) or [paraview](https://www.paraview.org/) to explore the 3D dispersions (NRRD). For instance we show electronic dispersions in the 1st Brillouin zone below.

![electronic dispersion energy iso-surface with silx](images/silx-view-cube.png)

#### Usage of web service via an API call

It is possible to programmatically send a request for a computation with a command such as:
```
curl --trace-ascii LOGFILE -F user_id=farhie -F calculator=QuantumEspresso -F smearing=semiconductor -F compute=electrons_3d -F 'dataset=@"/home/farhie/Downloads/Al.cif"' https://data-analysis.synchrotron-soleil.fr/cgi-bin/material-modeling.pl
```

The result is returned in `LOGFILE` (can be set as `/dev/stdout`), with lines (can span on two lines to be merged) at the end such as:
```
0000: Location: /data/material-modeling/Al.cif-QuantumEspresso-farhie-
0040: 20231006-155947/
```

which indicates the location of the calculation on the server. Append this path `/data/material-modeling/Al.cif-QuantumEspresso-farhie-20231006-155947/` to the server, here `https://data-analysis.synchrotron-soleil.fr/`.

--------------------------------------------------------------------------------

## Example: LaB6

We start a LaB<sub>6</sub> calculation to estimate the electronic and vibrational dispersions:

```
material-modeling examples/LaB6.cif -c phonons_3d+electrons_3d
```
which takes a few minutes with 16 cores.

We then get a full report written e.g. in the `/tmp` directory. It contains both images and data files. The most interesting data file is `properties.h5` which contains all calculation information.

![LaB6 eDOS](images/electronic_dos.svg)

![LaB6 e BandStructure](images/electronic_band_structure.svg)

![LaB6 vDOS](images/phonon_dos.svg)

![LaB6 phonon BandStructure](images/phonon_band_structure.svg)

However, if you need to visualize the 3D dispersion for electronic and vibrational bands, it is best to use the `electronic_band_structure_3d.h5` and `phonon_band_structure_3d.h5` files which have reshaped data (e.g. HKL (20,20,20)).

The 3D dispersions (HDF5 and NPY) can be viewed with [silx](https://www.silx.org/), selecting the `FREQ` group and a **Cube** plotting. Then set the dimensions 1-3 to `x,y,z` and the last one shall be the mode to view. 

![LaB6 electronic dispersion energy iso-surface](images/silx-view-cube.png) 

The phonon spectrum can be viewed as iso-surfaces for each mode as well as $S(\vec{q},\omega)$ and powder averages.

![LaB6 phonon BandStructure_powder](images/phonon_band_structure_3d_Sqw_powder_neutron_LaB6.svg)

Last, projections of these dispersions for `h=0` are rendered, and can be rotated.

## Example: SrTiO3

We compute the electronic and vibrational dispersions for the SrTiO<sub>3</sub> perovskite.

```
material-modeling examples/SrTiO3.cif -c phonons_3d+electrons_3d
```
which takes 5-20 minutes depending on your computer.

We obtain a full report for the calculation, with nice 3D views to rotate around (using _plotly_). The ARPES and vibrational S(q,w) on the `h=0` plane are shown.

![SrTiO3 ARPES h=0](images/SrTiO3-arpes.png)

![SrTiO3 S(q,w) xrays h=0](images/SrTiO3-sqw-x.png)

## Issues

If you see error messages such as:
```
failed to open /dev/dri/renderD129: Permission denied
No protocol specified
```
then prepend `DISPLAY= HWLOC_COMPONENTS=-gl` to the executable command. This error is triggered by OpenMPI via SSH authorizing an X display with OpenGL. The actual command would then be:
```
DISPLAY= HWLOC_COMPONENTS=-gl material-modeling Al.cif ...
```

## Performance

:warning: This service is only meant to provide a quick estimate of the electronic and vibrational dispersions. Results are not meant to be accurate.

This script has been designed to provide an easy use (without complex DFT/ab-inito configuration).

We show below the typical computation times, in [s], obtained for Aluminium, using 2 cores. QuantumEspresso is an excellent choice.


Al          | GPAW    | Quantum Espresso  | VASP
------------|---------|-------------------|-----------
e-          | 22      | 3                 | 11
e-    3d    | 156     | 14                | 206
e- ph       | 1072    | 21                | 172
e- ph 3d    | 1204    | 32                | 371


## Credits

Powered by [ASE](https://wiki.fysik.dtu.dk/ase/index.html) and [PhonoPy](https://github.com/phonopy/phonopy). 3D plots by [Plotly](https://plotly.com/). (c) Synchrotron SOLEIL, France. GPL3 license.
See https://gitlab.com/soleil-data-treatment/soleil-software-projects/material-modeling-service

---
(c) 2023- E. Farhi - [Synchrotron SOLEIL/GRADES](https://www.synchrotron-soleil.fr/en/know-how/user-supports/scientific-data-analysis)
